import styled from "styled-components";

export const FlexBox = styled.div `
                display:flex;
                flex: ${props => !props.flex ? "" : props.flex };
                flex-direction: ${props => !props.flexDirection ? "" : props.flexDirection };
                justify-content:${props => !props.justifyContent ? "" : props.justifyContent};
                align-items:${props => !props.alignItems ? "" : props.alignItems};
                align-self:${props => !props.alignSelf ? "" : props.alignSelf};
                width: 100%;
                height: 100%;
`;
export const ErrorLabel = styled.span `
    font-size: 12px;
    font-family:'Roboto';
    color: rgba(255,0,0,0.8);
    padding: 5px 10px;
    margin-top:10px;
    margin-bottom:10px;
`;

export const StyledInput = styled.input `
    height:40px;
    color: #333;
    font-size:14px;
    font-family:Roboto;
    padding: 5px 10px;
    border-radius: 4px;
    border:none;
    min-width:350px;
    border:1px solid #999;
    box-sizing: border-box;
    :focus{
      outline:none;
      box-shadow:  rgba(0,0,0,0.5) 0px 0px 5px 0px ;
    }
    :disabled {
    background: #dddddd;
    }
`;

export const StyledHeader = styled.h1 `
    text-align: center;
    color:#0073ff;
    font-family:Roboto;
    font-size:28px;
    font-weight: bold;
    line-height:32px;
    min-width:150px;
    margin-bottom:15px;
`;


export const StyledLabel = styled.label `
    color:#333;
    height:40px;
    font-size:14px;
    font-weight: bold;
    line-height:40px;
    margin: 0px 15px;
    font-family:Roboto;
`;

export const StyledButton = styled.button `
    cursor:pointer;
    transition: background .5s ease;
    color:#fff;
    font-size:18px;
    text-align:center;
    font-family:Roboto;
    font-size:16px;
    font-weight:bold;
    text-shadow: 0px 0px 3px #333;
    background: #0073ff;
    height:40px;
    border-radius:5px;
    min-width:150px;
    margin-left:15px;
    border: none;
    :hover{
      opacity:.8;
    }
    :focus{
        outline:none;
    }
    :active{
      transform: scale(0.95,0.95);
      background: #0073ff;
    }
    :disabled {
      background: #dddddd;
    }
`;

export const Paper = styled.div `
    width: 100%;
    box-shadow: 0px 0px 5px 0px #666;
    padding:10px;
    border-radius: 5px;
`;

export default {
  FlexBox,
  ErrorLabel,
  StyledInput,
  StyledLabel,
  StyledButton,
  Paper,
  StyledHeader
}
