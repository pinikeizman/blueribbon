const ls = require('local-storage');

export const USERS_KEY = 'users';

export const initializeLocalStorageDb = ()=>{
    const initialUser = {
            nickname:"SuperUser",
            username:"user@bluerbn.com",
            password:"5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5"
    }
    const users = ls.get(USERS_KEY) || {};
    
    if(!users[initialUser.username]){
        users[initialUser.username] = initialUser;
        ls.set(USERS_KEY,users);
    }
}

export const updateUserDraggablePosition = () => {

}

export const getUsers = () => {
    const users = ls.get(USERS_KEY);
    if (!users) {
        const initialState = {
            "user@rapidapi.com": {
                nickname: "SuperUser",
                username: "user@rapidapi.com",
                password: "5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5"
            }
        };
        setUsers(initialState)
        return initialState;
    }
    return users;
}
export const setUsers = (users) => {
    return ls.set(USERS_KEY, users);
}

export const getUser = (id) => {
    return getUsers() && getUsers()[id];
}
export const setUser = (userToUpdate) => {
    const users = getUsers();
    users[userToUpdate.username] = userToUpdate;
    setUsers(users);
}

export const updateUser = (userToUpdate) => {
    const users = getUsers();
    users[userToUpdate.username] = {...users[userToUpdate.username], ...userToUpdate};
    setUsers(users);
}
export const addUsers = (user) => {
    const currentUsers = getUsers();
    ls.set(USERS_KEY,
        {
            ...currentUsers,
            ...user
        }
    );
}
