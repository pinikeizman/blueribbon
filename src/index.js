import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {createStore, applyMiddleware, combineReducers} from 'redux'
import {Provider} from 'react-redux'
import middlewares from './middlewares/';
import root from './reducers';
import {initializeLocalStorageDb} from "./api/userApi";
import 'react-select/dist/react-select.css';
import reducers from './reducers/index';

console.log(reducers);

const store = createStore(combineReducers(reducers), applyMiddleware(...middlewares));

window.store = store;

initializeLocalStorageDb();

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
