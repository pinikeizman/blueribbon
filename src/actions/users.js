import actions from '../actions/index';

export const setCurrentUser = (user) => ({
    type: actions.SET_USER,
    payload: {user}
});