import actions from '../actions/index';
import _ from "lodash";

export const addFlight = (flight) => {
    return ({
        type: actions.ADD_FLIGHT,
        payload: {
            flight:{
                ...flight,
                id: _.uniqueId()
            }
        }
    })
};