import React from "react";
import styled, {keyframes} from 'styled-components';
import {FlexBox} from "../Styled/styled.js";

const SnackbarAnimation = (props)=>keyframes`
  from {
      opacity: ${props.opacityStart};
      transform: ${props.translateStart};
    }

    to {
      opacity: ${props.opacityEnd};
      transform: ${props.translateEnd};
    }
`

const StyledSnackbar = styled.div`
  position: fixed;
  font-family:Roboto;
  min-width:350px;
  ${props=>props.position}:0;
  animation: ${props => SnackbarAnimation(props.animation)} 350ms ease forwards;
`

class SnackBar extends React.Component{
  state = {
    timeout: this.props.timeout || 4000,
    position: this.props.position || "",
    open: this.props.open,
    opacityStart: "0",
    translateStart: "translate(0px, 0px)",
    opacityEnd:"1",
    translateEnd:"translate(0px, -10px)"

  }

  componentDidMount(){
    this.setCloseStateToTrue = setTimeout(() => {
      this.setState({
        open: false,
        opacityStart: "1",
        translateStart: "translate(0px, -10px)",
        opacityEnd:"0",
        translateEnd:"translate(0px, 0px)"
      });
    }, this.props.timeout || 4000);
  }

  componentWillUnmount(){
    clearTimeout(this.setCloseStateToTrue);
  }

  render(){
    return <StyledSnackbar
              {...this.props}
              position={ this.props.position || "top"  }
              animation={{
                opacityStart: this.state.opacityStart,
                translateStart: this.state.translateStart,
                opacityEnd: this.state.opacityEnd,
                translateEnd: this.state.translateEnd
              }}
              style={{
                border:"1px rgb(152, 150, 150) solid",
                height:40,
                borderRadius:5,
                fontColor:"rgb(152, 150, 150)",
                padding:5,
                boxSizing:"border-box",
                background: this.props.facad == "error" ? "rgba(255,0,0,.1)" : "rgba(0,255,0,.1)",
              }}
            >
                  <FlexBox justifyContent="center" alignItems="center">
                    {this.props.children}
                  </FlexBox>
            </StyledSnackbar>;
  }

}

export default (props)=>{

  return(
    props.open ? <SnackBar {...props}/> : null
  );
}
