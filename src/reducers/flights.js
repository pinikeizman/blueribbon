import actions from '../actions/';
import _ from "lodash";

const initialState = {
};

export default function users(state = initialState, action) {
    switch (action.type) {
        case actions.ADD_FLIGHT:
            return { ...state, [action.payload.flight.id]: action.payload.flight};
        default:
            return state;
    }
}