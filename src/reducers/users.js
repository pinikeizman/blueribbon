import actions from '../actions/';
import _ from "lodash";

const initialState = {
};

export default function users(state = initialState, action) {
    switch (action.type) {
        case actions.SET_USER:
            return action.payload.user;
        default:
            return state;
    }
}