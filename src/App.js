import React, {Component} from 'react';
import TabComponent from './TabComponent/TabComponent';
import Login from "./components/Login/Login";
import {connect} from "react-redux";
import _ from "lodash";
import AddNewFlight from "./components/AddNewFlight/AddNewFlight";
import ShowFlights from "./components/ShowFlights/ShowFlights";

class App extends Component {
    tabs = [
        {
            title: "Add New Flight",
            component: <AddNewFlight/>
        },
        {
            title: "ShowFlights",
            component: <ShowFlights/>

        }
    ];

    render() {
        if(false){//_.isEmpty(this.props.currentUser)){
            return <Login/>
        }
        return (
            <div className="App" style={{height:1400}}>
                <div className="HeaderContainer">
                </div>
                <div className="ContentContainer" style={{padding:20}}>
                    <TabComponent tabs={this.tabs}/>
                </div>
            </div>
        );
    }
}

// const mapStateToProps = (state, props) => ({
//     vasts: state.vasts
// });
const mapStateToProps = (state) => {
    return({
        currentUser: state.user
    });
}

export default connect(mapStateToProps)(App);
