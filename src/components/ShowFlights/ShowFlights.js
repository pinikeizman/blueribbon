import React from "react";
import _ from "lodash";
import styled from 'styled-components';
import {connect} from "react-redux";
import {inputs} from "../AddNewFlight/AddNewFlight";
import { FlexBox, StyledHeader } from "../../Styled/styled";


const StyledTable = styled.table`
margin-top:25px;
min-width: 500px;
font-family: Roboto;
border-collapse: collapse;

th,td{
  font-size: 14px;
  font-family: Roboto;
  line-height:28px;
  padding:0px 5px;
}
th{
  font-weight: bold;
}
td{
  font-size: 14px;
}
thead tr{
  background: #EEE;
}
tbody tr:nth-child(2n){
  background: #aed3ff4f;
}
`

const tableHeadGenerator = (tableHeadList) =>
  ( <tr key={ _.uniqueId() }>
  {
    tableHeadList.map(Header =>
      <th key={ _.uniqueId() }>{Header}</th>)
  }
  </tr> );

const tableBodyGenerator = (flightsList) =>
  flightsList.map( flight =>
    <tr key={ _.uniqueId() }>
      {
        Object.keys(inputs).map(key=><td key={ _.uniqueId() }>{inputs[key].transform && inputs[key].transform(flight[key]) || flight[key]}</td>)
      }
    </tr>
  );


const ShowFlights = (props) => {
  const { flights } = props;
  const flightsList = Object.keys(flights).map((key)=>flights[key]);
  if(flightsList.length == 0){
   return <FlexBox justifyContent="center">
        <StyledHeader>No flights available</StyledHeader>
     </FlexBox>;
  }
  return ( 
  <FlexBox justifyContent="center">
    <StyledHeader>Flights:</StyledHeader>
    <StyledTable>
            <thead>
              {
                tableHeadGenerator(Object.keys(inputs).map(key=>inputs[key].label))
              }
            </thead>
            <tbody>
            {
              tableBodyGenerator(flightsList)
            }
            </tbody>
    </StyledTable> 
   </FlexBox>);
}
const mapStateToProps = (state)=>({
  flights: state.flights
});

export default connect(mapStateToProps)(ShowFlights);
