import React from "react";
import _ from "lodash";
import styled from 'styled-components';
import {StyledInput,StyledLabel,FlexBox,StyledHeader,StyledButton,ErrorLabel} from "../../Styled/styled.js";
import * as api from "../../api/userApi";
import * as userActionCreators from '../../actions/users';
import {connect} from "react-redux";

const sha256 = require("sha256");

const inputsKeys = {
  passwordKey: "password",
  usernameKey: "username"
}

class Login extends React.Component{ 
  state = {
    [inputsKeys.usernameKey]: "",
    [inputsKeys.passwordKey]: ""
  }
  
  handleInputOnChnage = (value, inputName) => {
    switch (inputName) {
        case inputsKeys.usernameKey:
            this.setState({[inputsKeys.usernameKey]: value});
            break;
        case inputsKeys.passwordKey:
            this.setState({[inputsKeys.passwordKey]: value});
            break;
        default:
    }
    this.setState({loginError: ""});
  }

  login = () => {
    const user = api.getUser(this.state[inputsKeys.usernameKey]);
    if (_.get(user, inputsKeys.passwordKey) === sha256(this.state[inputsKeys.passwordKey])) {
        this.props.setCurrentUser(user);
        return;
    }
    this.setState({loginError: "Please provid valid credentials."});
  }

  render(){
    return( 
      <FlexBox alignItems="center" flexDirection="column">
          <StyledHeader>Login</StyledHeader>
          <FlexBox justifyContent="center" style={{marginBottom:15}}>
            <StyledLabel>Username:</StyledLabel>
            <StyledInput 
                         value={this.state[inputsKeys.usernameKey]}
                         onChange={(e) => this.handleInputOnChnage(e.target.value, inputsKeys.usernameKey)}
                         placeholder="Username"
                         type="text"/>            
          </FlexBox>
          <FlexBox justifyContent="center" >
            <StyledLabel>Password:</StyledLabel>
            <StyledInput className="form-input"
                         value={this.state[inputsKeys.passwordKey]}
                         onChange={(e) => this.handleInputOnChnage(e.target.value, inputsKeys.passwordKey)}
                         placeholder="Password"
                         style={{marginBottom: 10}}
                         type="password"/>
          </FlexBox>
          <StyledButton onClick={this.login} style={{marginTop:15}}>
            Submit
          </StyledButton>
          {
                this.state.loginError ?
                    <ErrorLabel>
                        {this.state.loginError}
                    </ErrorLabel>
                    : null
          }
      </FlexBox>);
    }
}

export default connect(null, {
  setCurrentUser: userActionCreators.setCurrentUser
})(Login);
