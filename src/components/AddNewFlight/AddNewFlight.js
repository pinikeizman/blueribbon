import React from "react";
import _ from "lodash";
import {StyledInput,StyledLabel,FlexBox,StyledHeader,StyledButton, ErrorLabel} from "../../Styled/styled.js";
import * as api from "../../api.js";
import Select from 'react-select';
import styled from 'styled-components';
import {connect} from "react-redux";
import * as flightActionCreator from '../../actions/flights';

var moment = require('moment');

export const inputs =  {
  from: {
    label: "From",
    validate: (value)=> !_.isEmpty(value)
  },
  to: {
    label: "To",
    validate: (value)=> !_.isEmpty(value)
  },
  departureTime: {
    label: "Departure Time​",
    type: "datetime-local",
    validate: (value)=> moment(value).isValid(),
    transform: (value)=> moment().format('MMMM Do YYYY, h:mm:ss a')
  },
  landingTime: {
    label: "Landing Time​",
    type: "datetime-local",
    validate: (value)=> moment(value).isValid(),
    transform: (value)=> moment().format('MMMM Do YYYY, h:mm:ss a')
  }
}

class AddNewFlight extends React.Component {
    state = {
      errors: []
    };

    handleInputChange = (value, feild) => {
      const fieldValues = { ...this.state , [feild]:value };
      this.setState({ [feild]: value },()=>{
        const errors = this.validate(fieldValues);
        this.setState({errors});
      });
    };

    validate = (values)=>{
      return Object.keys(inputs).reduce((state, key, index)=> inputs[key].validate(values[key] || "") ? state : [...state , `Please enter a valid ${inputs[key].label} field.`],[])
    }

    submit = (csvId) => {
      if(this.state.errors.length != 0 ){
        return;
      }
console.log(this.state);
      this.props.addFlight(this.state);
    }

    render() {
      return (
        <FlexBox alignItems="center" flexDirection="column">
          <StyledHeader>Add New Flight</StyledHeader>
          <FlexBox alignItems="center" flexDirection="column" style={{width: '50%'}}>

          {
            Object.keys(inputs).map(key=> 
            <FlexBox key={key} justifyContent="space-between" style={{marginBottom:15}}>
              <StyledLabel>{inputs[key].label}</StyledLabel>
              <StyledInput  value={this.state[key] || ""}
                            onChange={(e) => this.handleInputChange(e.target.value, key)}
                            placeholder={inputs[key].label}
                            type={inputs[key].type || "text"}/>  
            </FlexBox>)
          }
          </FlexBox>
        {this.state.errors ? <ul style={{color:"red"}}>{this.state.errors.map(error=><li key={_.uniqueId()}>{error}</li>)}</ul> : null }
        <StyledButton onClick={this.submit} style={{marginTop:15}}>
            Submit
          </StyledButton>
        </FlexBox>
       );
      }
    }
export default connect(null,{
  addFlight: flightActionCreator.addFlight
})(AddNewFlight);